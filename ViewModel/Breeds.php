<?php


namespace Gamma\Dogs\ViewModel;

use Exception;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\View\Element\Block\ArgumentInterface;

class Breeds implements ArgumentInterface
{
    /**
     * @var \Gamma\Dogs\Model\Breeds
     */
    protected $breeds;

    /**
     * @var RequestInterface
     */
    protected $request;

    public function __construct(
        \Gamma\Dogs\Model\Breeds $breeds,
        RequestInterface $request
    )
    {
        $this->breeds = $breeds;
        $this->request = $request;
    }

    public function getList(){
        return $this->breeds->getBreedsList();
    }

    public function getBreed(){

        $requestedBreed = $this->request->getParam('breed');

        if ($requestedBreed) {
            try {
                return $this->breeds->getBreed($requestedBreed);
            } catch (Exception $e) {
                print_r($e->getMessage());
                return null;
            }
        }

        return null;
    }
}