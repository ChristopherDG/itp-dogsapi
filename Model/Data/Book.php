<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BookInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Book extends AbstractSimpleObject implements BookInterface
{

    public function getTitle(): string
    {
        return $this->_get(self::TITLE);
    }

    public function setTitle(string $title): BookInterface
    {
        return $this->setData(self::TITLE, $title);
    }

    public function getId(): string
    {
        return $this->_get(self::ID);
    }

    public function setId(string $id): BookInterface
    {
        return $this->setData(self::ID, $id);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): BookInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getAuthor(): string
    {
        return $this->_get(self::AUTHOR);
    }

    public function setAuthor(string $author): BookInterface
    {
        return $this->setData(self::AUTHOR, $author);
    }

    public function getLink(): string
    {
        return $this->_get(self::LINK);
    }

    public function setLink(string $link): BookInterface
    {
        return $this->setData(self::LINK, $link);
    }
}