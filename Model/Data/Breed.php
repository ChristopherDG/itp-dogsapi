<?php


namespace Gamma\Dogs\Model\Data;


use Gamma\Dogs\Api\Data\BreedInterface;
use Magento\Framework\Api\AbstractSimpleObject;

class Breed extends AbstractSimpleObject implements BreedInterface
{

    public function getName(): string
    {
        return $this->_get(self::NAME);
    }

    public function setName(string $name): BreedInterface
    {
        return $this->setData(self::NAME, $name);
    }

    public function getSubBreeds(): array
    {
        return $this->_get(self::SUB_BREEDS);
    }

    public function setSubBreeds(array $sub_Breeds): BreedInterface
    {
        return $this->setData(self::SUB_BREEDS, $sub_Breeds);
    }

    public function getImage(): string
    {
        return $this->_get(self::IMAGE);
    }

    public function setImage(string $image): BreedInterface
    {
        return $this->setData(self::IMAGE, $image);
    }

    public function getBooks(): array
    {
        return $this->_get(self::BOOKS);
    }

    public function setBooks(array $books): BreedInterface
    {
        return $this->setData(self::BOOKS, $books);
    }
}