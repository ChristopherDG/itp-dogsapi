<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\ConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class Connection implements ConnectionInterface
{

    protected $baseUrl;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://dog.ceo/api'
    )
    {
        $this->baseUrl = $baseUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function getInfo($path): array
    {
        $requestPath = "{$this->baseUrl}/{$path}";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response);
    }
}