<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\BookConnectionInterface;
use Magento\Framework\HTTP\Client\CurlFactory;
use Magento\Framework\Serialize\Serializer\Json;

class BookConnection implements BookConnectionInterface
{
    protected $baseUrl;

    /**
     * @var Json
     */
    protected $jsonSerializer;

    /**
     * @var CurlFactory
     */
    protected $httpClient;

    public function __construct(
        Json $jsonSerializer,
        CurlFactory $httpClient,
        string $baseUrl = 'https://www.googleapis.com'
    )
    {
        $this->baseUrl = $baseUrl;
        $this->jsonSerializer = $jsonSerializer;
        $this->httpClient = $httpClient;
    }

    public function getBooks($breed): array
    {
        $requestPath = "{$this->baseUrl}/books/v1/volumes?q={$breed}+Dog";

        $client = $this->httpClient->create();

        $client->get($requestPath);

        $response = $client->getBody();

        return $this->jsonSerializer->unserialize($response)['items'];
    }
}