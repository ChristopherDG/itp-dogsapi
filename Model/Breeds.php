<?php


namespace Gamma\Dogs\Model;

use Gamma\Dogs\Api\ConnectionInterface;
use Gamma\Dogs\Api\BreedsInterface;
use Gamma\Dogs\Api\BookConnectionInterface;
use Gamma\Dogs\Api\Data\BreedInterfaceFactory;
use Gamma\Dogs\Api\Data\BreedInterface;
use Gamma\Dogs\Api\Data\BookInterfaceFactory;

class Breeds implements BreedsInterface
{
    /**
     * @var Connection
     */
    protected $connection;

    /**
     * @var Connection
     */
    protected $bookConnection;
    /**
     * @var BreedInterfaceFactory
     */
    protected $breedFactory;

    /**
     * @var BookInterfaceFactory
     */
    protected $bookFactory;

    public function __construct(
        ConnectionInterface $connection,
        BookConnectionInterface $bookConnection,
        BreedInterfaceFactory $breedFactory,
        BookInterfaceFactory $bookFactory
    ){
      $this->connection = $connection;
      $this->bookConnection = $bookConnection;
      $this->breedFactory = $breedFactory;
      $this->bookFactory = $bookFactory;
    }

    public function getBreedsList(): array
    {
        $breedsData = $this->connection->getInfo('breeds/list/all');

        return array_keys($breedsData['message']);

    }

    public function getBreed(string $name): BreedInterface{
        $breed = $this->breedFactory->create();

        $breedImage = $this->processImage($name);
        $breedSubBreeds = $this->processSubBreeds($name);
        $breedBooks = $this->processBooks($name);

        $breed->setImage($breedImage)
            ->setName(ucfirst($name))
            ->setSubBreeds($breedSubBreeds)
            ->setBooks($breedBooks);

        return $breed;
    }

    public function processImage(string $name){
        $breedsData = $this->connection->getInfo("breed/{$name}/images");
        return $breedsData['message'][0];
    }

    public function processSubBreeds(string $name){
        $breedsData = $this->connection->getInfo('breeds/list/all');
        return $breedsData['message'][$name];
    }

    public function processBooks(string $name){
        $booksData = $this->bookConnection->getBooks(ucfirst($name));

        $books = Array();
        for($i = 0; $i < 3 ; $i++){
            $bookInfo = $booksData[$i]['volumeInfo'];

            $bookIns = $this->bookFactory->create();

            $bookIns->setId($bookInfo['industryIdentifiers'][0]['identifier'])
                ->setTitle($bookInfo['title'])
                ->setAuthor($bookInfo['authors'][0])
                ->setImage($bookInfo['imageLinks']['thumbnail'])
                ->setLink($booksData[$i]['accessInfo']['webReaderLink']);

            array_push($books, $bookIns);
        }

        return $books;
    }

}