<?php


namespace Gamma\Dogs\Api;


interface BookConnectionInterface
{
    public function getBooks($breed): array;
}